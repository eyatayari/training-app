import { Component, OnInit ,} from '@angular/core';
import {FakeItemSessionService} from "../fake-item-session.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
    providers: [FakeItemSessionService],
})
export class AdminComponent implements OnInit {
    imageSrc = 'assets/add.png';
    constructor() { }

  ngOnInit(): void {
  }

}

import { TestBed } from '@angular/core/testing';

import { FakeItemSessionService } from './fake-item-session.service';

describe('FakeItemSessionService', () => {
  let service: FakeItemSessionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FakeItemSessionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

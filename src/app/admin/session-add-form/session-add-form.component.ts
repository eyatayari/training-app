import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {FakeItemSessionService} from "../fake-item-session.service";

@Component({
  selector: 'app-session-add-form',
  templateUrl: './session-add-form.component.html',
  styleUrls: ['./session-add-form.component.css']
})
export class SessionAddFormComponent implements OnInit {

  constructor(private sessionItemService : FakeItemSessionService) { }

  ngOnInit(): void {
  }
    addSession(sessionItem:NgForm):void {
        console.log("Logs...."+JSON.stringify(sessionItem.value));
        this.sessionItemService.add(sessionItem.value);
    }
}

import { Component, OnInit } from '@angular/core';
import {FakeItemSessionService} from "../fake-item-session.service";

@Component({
  selector: 'app-session-item-list',
  templateUrl: './session-item-list.component.html',
  styleUrls: ['./session-item-list.component.css']
})
export class SessionItemListComponent implements OnInit {
sessionItems : any;
  constructor(private sessionItemService : FakeItemSessionService) { }

  ngOnInit(): void {
  this.sessionItems=this.sessionItemService.get();
  }

}

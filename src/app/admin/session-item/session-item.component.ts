import {Component, Input, OnInit} from '@angular/core';
import {FakeItemSessionService} from "../fake-item-session.service";

@Component({
  selector: 'app-session-item',
  templateUrl: './session-item.component.html',
  styleUrls: ['./session-item.component.css']
})
export class SessionItemComponent implements OnInit {
@Input() session :any;
  constructor(private sessionItemService: FakeItemSessionService) { }

  ngOnInit(): void {

  }
  onDelete(){
    console.log(this.session);
    this.sessionItemService.delete(this.session);
  }


}

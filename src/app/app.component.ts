import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'training-app';
  
  /*firstSession={
    id:1,
    name: 'Formation web',
    track:'mean Stack',
    date:'19/10/2019',
    lieu:'Lyon',
    participants:0,
  }*/
}

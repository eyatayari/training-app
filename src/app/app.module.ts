import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SessionItemComponent } from './session-item/session-item.component';
import { SessionItemListComponent } from './session-item-list/session-item-list.component';
import { InscriptionDisabledDirective } from './inscription-disabled.directive';
import {FakeItemSessionService} from "./admin/fake-item-session.service";
// import { PageNotFoundComponent } from './page-not-found/page not-found.component';

import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from "@angular/common";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
const appRoutes: Routes = [
    {
        path: 'list',
        component: SessionItemListComponent},
    {
        path: 'admin',
        loadChildren: './admin/admin.module#AdminModule'
    },
     { path: '', redirectTo: '/list', pathMatch: 'full' },  { path: '**', component: PageNotFoundComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    SessionItemComponent,
    SessionItemListComponent,
    InscriptionDisabledDirective,
    PageNotFoundComponent,



  ],
  imports: [RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
  ),

     BrowserModule,
    AppRoutingModule
  ],
  providers: [FakeItemSessionService],
  bootstrap: [AppComponent]
})
export class AppModule { }

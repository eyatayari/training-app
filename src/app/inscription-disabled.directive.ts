import {Directive, HostBinding, Input} from '@angular/core';

@Directive({
  selector: '[appInscriptionDisabled]'
})
export class InscriptionDisabledDirective {
value : boolean = false;
  @HostBinding('class.link-is-disabled') status !: boolean;

  constructor() { }

}

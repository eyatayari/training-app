import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-session-item',
  templateUrl: './session-item.component.html',
  styleUrls: ['./session-item.component.css']
})
export class SessionItemComponent implements OnInit {
@Input()  session: any ;
  constructor() { }
  ngOnInit(): void {
    
  }
  inscrire():void {
    this.session.name ="formation web avancée";
    console.log('nouvelle inscription');
    this.session.participants+=1;
    if(this.session.participants>=20){
      this.session.isCompleted=true;
    }
  }

}
